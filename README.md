# Rial Exchange, Coin & Gold Rate FREE API.

[![Netlify Status](https://api.netlify.com/api/v1/badges/b4384925-e564-4865-922b-4bfbff02e75e/deploy-status)](https://app.netlify.com/sites/free-currency/deploys)

### Get Rial Exchange Rate
```sh
curl https://free-currency.netlify.app/.netlify/functions/server
```
### Get Coin Rate
```sh
curl https://free-currency.netlify.app/.netlify/functions/server/coin
```
### Get Gold Rate
```sh
curl https://free-currency.netlify.app/.netlify/functions/server/gold
```
