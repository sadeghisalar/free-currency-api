'use strict';
const express = require('express');
const request = require("request");
const serverless = require('serverless-http');
const app = express();
const bodyParser = require('body-parser');

const router = express.Router();

router.get('/', (req, res) => {
  let content = "";
  let result = [];
  let fetch =request(
    { uri: "https://www.tgju.org/currency?"+Date.now() },
    function(error, response, body) {
      content = body;
      let DomParser = require('dom-parser');
      let parser = new DomParser();

      let dom = parser.parseFromString(content);

      dom.getElementsByTagName('tr').forEach(function (tr) {

        if( tr.getAttribute('data-market-row') != null ){
          let temp = {};
          let name = tr.getAttribute('data-market-row');
          let price = tr.getElementsByTagName("td")[0].textContent;
          let label = tr.getElementsByClassName("mini-flag")[0].parentNode.textContent;
          label = label.trim();
          name = name.replace('price_','')
          name = name.replace('_rl','')
          price = price.replace(/,/gi,'')
          price = parseFloat(price)
          temp['name'] = name;
          temp['rial'] = price;
          temp['toman'] = price/10;
          temp['label'] = label;
          if(result.indexOf(temp) === -1){
            result.push(temp);
          }

        }
      })
      res.send(result)
      fetch = null;
      result = [];
      res.end();
    }
  )
});

app.get('/.netlify/functions/server/coin',function (req, res) {
  let content = "";
  let result = [];
  let fetch = request(
    { uri: "https://www.tgju.org/coin?"+Date.now() },
    function(error, response, body) {
      content = body;
      let DomParser = require('dom-parser');
      let parser = new DomParser();

      let dom = parser.parseFromString(content);

      dom.getElementsByTagName('tr').forEach(function (tr) {

        if( tr.getAttribute('data-market-row') != null ){
          let temp = {};
          let name = tr.getAttribute('data-market-row');
          let price = tr.getElementsByTagName("td")[0].textContent;
          let label = tr.getElementsByTagName("th")[0].innerHTML;
          label = label.trim();
          price = price.replace(/,/gi,'')
          price = parseFloat(price)
          temp['name'] = name;
          temp['rial'] = price;
          temp['toman'] = price/10;
          temp['label'] = label;
          if(result.indexOf(temp) === -1){
            result.push(temp);
          }

        }
      })
      res.send(result)
      fetch = null;
      result = [];
      res.end();
    }
  )
});

app.get('/.netlify/functions/server/gold',function (req, res) {
  let content = "";
  let result = [];
  let fetch = request(
    { uri: "https://www.tgju.org/gold-chart?"+Date.now() },
    function(error, response, body) {
      content = body;
      let DomParser = require('dom-parser');
      let parser = new DomParser();

      let dom = parser.parseFromString(content);

      dom.getElementsByTagName('tr').forEach(function (tr) {

        if( tr.getAttribute('data-market-row') != null ){
          let temp = {};
          let name = tr.getAttribute('data-market-row');
          let price = tr.getElementsByTagName("td")[0].textContent;
          let label = tr.getElementsByTagName("th")[0].innerHTML;
          label = label.trim();
          price = price.replace(/,/gi,'')
          price = parseFloat(price)
          temp['name'] = name;
          temp['rial'] = price;
          temp['toman'] = price/10;
          temp['label'] = label;
          if(result.indexOf(temp) === -1){
            result.push(temp);
          }

        }
      })
      res.send(result)
      fetch = null;
      result = [];
      res.end();
    }
  )
});

app.use(bodyParser.json());
app.use('/.netlify/functions/server', router);
module.exports = app;
module.exports.handler = serverless(app);
